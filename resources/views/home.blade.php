@extends('layouts.main')

@section('title', 'Home Page')

@section('description', 'Home Page')
@section('keywords', 'Home Page')

@section('content')

    <div class="content">
        <div class="container">

        <!-- START: PAGE CONTENT -->
            <section id="about" class="section section-about">
                <div class="animate-up">
                    <div class="section-box">
                        <div class="profile">
                            <div class="row">
                                <div class="col-xs-5">
                                    <div class="profile-photo"><img src="img/uploads/rs-photo-v1.jpg" alt="Robert Smith"/></div>
                                </div>
                                <div class="col-xs-7">
                                    <div class="profile-info">
                                        <div class="profile-preword"><span>Hello</span></div>
                                        <h1 class="profile-title"><span>I'm</span> Adam Wilson</h1>

                                        <h2 class="profile-position">SEO / Developer / Designer</h2></div>
                                        <dl class="profile-list">
                                            <dt>Age</dt>
                                            <dd>18</dd>
                                            <dt>Address</dt>
                                            <dd>80 Norwich Road, Wymondham, Norfolk, UK</dd>
                                            <dt>E-mail</dt>
                                            <dd><a href="mailto:info@creativedesignnorwich.co.uk">info@creativedesignnorwich.co.uk</a></dd>
                                            <dt>Phone</dt>
                                            <dd><a href="tel:07736 686680">07736 686680</a></dd>
                                            <dt>Freelance</dt>
                                            <dd>Since September, 2015</dd>
                                            <dt class="profile-vacation"><span>At College</span></dt>
                                            <dd class="profile-vacation"><i class="icon icon-calendar"></i>January 18, 2016</dd>
                                        </dl>
                                </div>
                            </div>
                        </div>
                        <div class="profile-social">
                            <ul class="social">
                                <li><a class="ripple-centered" href="https://www.facebook.com/08wilsona" target="_blank"><i class="icon icon-facebook"></i></a></li>
                                <li><a class="ripple-centered" href="https://twitter.com//its_adam97" target="_blank"><i class="icon icon-twitter"></i></a></li>
                                <li><a class="ripple-centered" href="https://uk.linkedin.com/in/08wilsona" target="_blank"><i class="icon icon-linkedin"></i></a></li>
                                <li><a class="ripple-centered" href="https://www.instagram.com/itsadam97" target="_blank"><i class="icon icon-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    
                    <div class="profile-btn">
                        <a class="btn btn-border ripple" href="/cv.docx">Download Resume</a>
                    </div>
                    
                    <div class="profile-text">
                        <p>Hello! I'm Adam Wilson. Web Developer specializing in back end development. Experienced with all stages of the development cycle for dynamic web projects. 
                        Well-versed in numerous programming languages including JavaScript, PHP, SQL, and Swift. Background in project management and customer relations.</p>
                    </div>
                </div>  
            </section><!-- #about -->
            
            <section id="skills" class="section section-skills">
                <div class="animate-up">
                    <h2 class="section-title">Proffessional  Skills</h2>
                    <div class="section-box">
                        <div class="row">                           
                            <div class="col-sm-6">
                                <div class="progress-bar">
                                    <div class="bar-data">
                                        <span class="bar-title">Brand Development</span>
                                        <span class="bar-value">85%</span>
                                    </div>
                                    <div class="bar-line">
                                        <span class="bar-fill" data-width="85%"></span>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6">
                                <div class="progress-bar">
                                    <div class="bar-data">
                                        <span class="bar-title">Photoshop</span>
                                        <span class="bar-value">75%</span>
                                    </div>
                                    <div class="bar-line">
                                        <span class="bar-fill" data-width="75%"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">                           
                            <div class="col-sm-6">
                                <div class="progress-bar">
                                    <div class="bar-data">
                                        <span class="bar-title">HTML5 & CSS3</span>
                                        <span class="bar-value">85%</span>
                                    </div>
                                    <div class="bar-line">
                                        <span class="bar-fill" data-width="85%"></span>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6">
                                <div class="progress-bar">
                                    <div class="bar-data">
                                        <span class="bar-title">Javascript</span>
                                        <span class="bar-value">35%</span>
                                    </div>
                                    <div class="bar-line">
                                        <span class="bar-fill" data-width="35%"></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">                           
                            <div class="col-sm-6">
                                <div class="progress-bar">
                                    <div class="bar-data">
                                        <span class="bar-title">Jquery</span>
                                        <span class="bar-value">25%</span>
                                    </div>
                                    <div class="bar-line">
                                        <span class="bar-fill" data-width="25%"></span>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6">
                                <div class="progress-bar">
                                    <div class="bar-data">
                                        <span class="bar-title">Ajax</span>
                                        <span class="bar-value">25%</span>
                                    </div>
                                    <div class="bar-line">
                                        <span class="bar-fill" data-width="25%"></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">                           
                            <div class="col-sm-6">
                                <div class="progress-bar">
                                    <div class="bar-data">
                                        <span class="bar-title">Bootstrap</span>
                                        <span class="bar-value">95%</span>
                                    </div>
                                    <div class="bar-line">
                                        <span class="bar-fill" data-width="95%"></span>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6">
                                <div class="progress-bar">
                                    <div class="bar-data">
                                        <span class="bar-title">Materialize</span>
                                        <span class="bar-value">85%</span>
                                    </div>
                                    <div class="bar-line">
                                        <span class="bar-fill" data-width="85%"></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">                           
                            <div class="col-sm-6">
                                <div class="progress-bar">
                                    <div class="bar-data">
                                        <span class="bar-title">PHP</span>
                                        <span class="bar-value">80%</span>
                                    </div>
                                    <div class="bar-line">
                                        <span class="bar-fill" data-width="80%"></span>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6">
                                <div class="progress-bar">
                                    <div class="bar-data">
                                        <span class="bar-title">MYSQL</span>
                                        <span class="bar-value">70%</span>
                                    </div>
                                    <div class="bar-line">
                                        <span class="bar-fill" data-width="70%"></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">                           
                            <div class="col-sm-6">
                                <div class="progress-bar">
                                    <div class="bar-data">
                                        <span class="bar-title">Wordpress</span>
                                        <span class="bar-value">90%</span>
                                    </div>
                                    <div class="bar-line">
                                        <span class="bar-fill" data-width="90%"></span>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6">
                                <div class="progress-bar">
                                    <div class="bar-data">
                                        <span class="bar-title">Laravel</span>
                                        <span class="bar-value">90%</span>
                                    </div>
                                    <div class="bar-line">
                                        <span class="bar-fill" data-width="90%"></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">                           
                            <div class="col-sm-6">
                                <div class="progress-bar">
                                    <div class="bar-data">
                                        <span class="bar-title">SEO</span>
                                        <span class="bar-value">85%</span>
                                    </div>
                                    <div class="bar-line">
                                        <span class="bar-fill" data-width="85%"></span>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-6">
                                <div class="progress-bar">
                                    <div class="bar-data">
                                        <span class="bar-title">Swift</span>
                                        <span class="bar-value">40%</span>
                                    </div>
                                    <div class="bar-line">
                                        <span class="bar-fill" data-width="40%"></span>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>  
            </section><!-- #skills -->                              
            
            <section id="portfolio" class="section section-portfolio">
                <div class="animate-up">
                    <h2 class="section-title">Portfolio</h2>

                    <div class="filter">
                        <div class="filter-inner">
                            <div class="filter-btn-group">
                                <button data-filter="*">All</button>
                                <button data-filter=".photography">Photography</button>
                                <button data-filter=".nature">Websites</button>
                                <button data-filter=".nature">Themes</button>
                            </div>
                            <div class="filter-bar">
                                <span class="filter-bar-line"></span>
                            </div>
                        </div>
                    </div>

                    <div class="grid">
                        <div class="grid-sizer"></div>
                        
                        <div class="grid-item size22 photography">
                            <div class="grid-box">
                                <figure class="portfolio-figure">
                                    <img src="img/uploads/portfolio/portfolio-thumb-05-610x600.jpg" alt=""/>
                                    <figcaption class="portfolio-caption">
                                        <div class="portfolio-caption-inner">
                                            <h3 class="portfolio-title">Street Photography</h3>
                                            <h4 class="portfolio-cat">Photography</h4>

                                            <div class="btn-group">
                                                <a class="btn-link" href="http://bit.ly/1YtB8he" target="_blank"><i class="icon icon-link"></i></a>
                                                <a class="portfolioFancybox btn-zoom" data-fancybox-group="portfolioFancybox1" href="#portfolio1-inline1"><i class="icon icon-eye"></i></a>
                                                <a class="portfolioFancybox hidden" data-fancybox-group="portfolioFancybox1" href="#portfolio1-inline2"></a>
                                                <a class="portfolioFancybox hidden" data-fancybox-group="portfolioFancybox1" href="#portfolio1-inline3"></a>
                                            </div>
                                        </div>
                                    </figcaption>
                                </figure>
                                
                                <!-- Start: Portfolio Inline Boxes -->
                                <div id="portfolio1-inline1" class="fancybox-inline-box">
                                    <img class="inline-img" src="img/uploads/portfolio/portfolio-thumb-05-large.jpg" alt=""/>
                                    <div class="inline-cont">
                                        <h2 class="inline-title">Street photography is photography that features the chance encounters and random accidents within public places.</h2>
                                        <div class="inline-text">
                                            <p>Street photography does not necessitate the presence of a street or even the urban environment. Though people usually feature directly, street photography might be absent of people and can be an object or environment where the image projects a decidedly human character in facsimile or aesthetic.</p>
                                        </div>
                                    </div>
                                </div>                                                              
                                
                                <div id="portfolio1-inline2" class="fancybox-inline-box">
                                    <img class="inline-img" src="img/uploads/portfolio/portfolio-thumb-01-large.jpg" alt=""/>
                                    <div class="inline-cont">
                                        <div class="inline-text">
                                            <h2 class="inline-title">Framing and timing</h2>
                                            <p>Framing and timing can be key aspects of the craft with the aim of some street photography being to create images at a decisive or poignant moment. Street photography can focus on emotions displayed, thereby also recording people's history from an emotional point of view.</p>
                                        </div>
                                    </div>
                                </div>
                                
                                <div id="portfolio1-inline3" class="fancybox-inline-box">
                                    <div class="inline-embed">                                      
                                        <iframe class="inline-embed-item" src="https://player.vimeo.com/video/118244244" allowfullscreen></iframe>
                                    </div>
                                    <div class="inline-cont">
                                        <div class="inline-text">
                                            <h2 class="inline-title">A Look Into Documenting Street Fashion Photography</h2>
                                            <p>HB Nam is an internationally known street fashion photographer. In this Leica Camera Portrait, Nam explains how he started in photography and what photography means to him. For Nam, it's about documenting what's around him and the concentration required to achieve a good shot.</p>
                                        </div>
                                    </div>
                                </div>
                                <!-- End: Portfolio Inline Boxes -->
                            </div>
                        </div><!-- .grid-item -->           
                    
                        <div class="grid-item size11 bridge">
                            <div class="grid-box">
                                <figure class="portfolio-figure">
                                    <img src="img/uploads/portfolio/portfolio-thumb-11-289x281.jpg" alt=""/>
                                    <figcaption class="portfolio-caption">
                                        <div class="portfolio-caption-inner">
                                            <h3 class="portfolio-title">Suspension Bridge</h3>
                                            <h4 class="portfolio-cat">Bridge</h4>

                                            <div class="btn-group">
                                                <a class="btn-link" href="http://bit.ly/1YtB8he" target="_blank"><i class="icon icon-link"></i></a>
                                                <a class="portfolioFancybox btn-zoom" data-fancybox-group="portfolioFancybox2" href="#portfolio2-inline1"><i class="icon icon-eye"></i></a>                                                 
                                            </div>
                                        </div>
                                    </figcaption>
                                </figure>                                                                                       
                                
                                <!-- Start: Portfolio Inline Boxes -->                                                              
                                <div id="portfolio2-inline1" class="fancybox-inline-box">                               
                                    <div class="inline-cont">
                                        <h2 class="inline-title">Suspension Bridges - Design Technology</h2>
                                        <div class="inline-text">
                                            <p>Suspension bridges in their simplest form were originally made from rope and wood.
                                            Modern suspension bridges use a box section roadway supported by high tensile strength cables. 
                                            In the early nineteenth century, suspension bridges used iron chains for cables. The high tensile cables used in most modern suspension 
                                            bridges were introduced in the late nineteenth century.<br/>
                                            Today, the cables are made of thousands of individual steel wires bound tightly together. Steel, which is very strong under tension, is 
                                            an ideal material for cables; a single steel wire, only 0.1 inch thick, can support over half a ton without breaking.</p>
                                            <p>Light, and strong, suspension bridges can span distances from 2,000 to 7,000 feet far longer than any other kind of bridge. They are 
                                            ideal for covering busy waterways.</p>
                                            <p>With any bridge project the choice of materials and form usually comes down to cost.
                                            Suspension bridges tend to be the most expensive to build. A suspension bridge suspends the roadway from huge main cables, which extend 
                                            from one end of the bridge to the other. These cables rest on top of high towers and have to be securely anchored into the bank at either 
                                            end of the bridge. The towers enable the main cables to be draped over long distances. Most of the weight or load of the bridge is 
                                            transferred by the cables to the anchorage systems. These are imbedded in either solid rock or huge concrete blocks. Inside the anchorages, 
                                            the cables are spread over a large area to evenly distribute the load and to prevent the cables from breaking free.</p>
                                            <p>The Arthashastra of Kautilya mentions the construction of dams and bridges.A Mauryan bridge near Girnar was surveyed by James Princep. 
                                            The bridge was swept away during a flood, and later repaired by Puspagupta, the chief architect of emperor Chandragupta I. The bridge 
                                            also fell under the care of the Yavana Tushaspa, and the Satrap Rudra Daman. The use of stronger bridges using plaited bamboo and iron 
                                            chain was visible in India by about the 4th century. A number of bridges, both for military and commercial purposes, were constructed by 
                                            the Mughal administration in India.</p>
                                        </div>
                                    </div>
                                </div>
                                <!-- End: Portfolio Inline Boxes -->
                            </div>
                        </div><!-- .grid-item -->
                        
                        <div class="grid-item size11 nature photography">
                            <div class="grid-box">
                                <figure class="portfolio-figure">
                                    <img src="img/uploads/portfolio/portfolio-thumb-08-289x281.jpg" alt=""/>
                                    <figcaption class="portfolio-caption">
                                        <div class="portfolio-caption-inner">
                                            <h3 class="portfolio-title">Rocky Mountains</h3>
                                            <h4 class="portfolio-cat">Nature, Photography</h4>

                                            <div class="btn-group">
                                                <a class="btn-link" href="http://bit.ly/1YtB8he" target="_blank"><i class="icon icon-link"></i></a>
                                                <a class="portfolioFancybox btn-zoom" data-fancybox-group="portfolioFancybox3" href="#portfolio3-inline1"><i class="icon icon-eye"></i></a>
                                                <a class="portfolioFancybox hidden" data-fancybox-group="portfolioFancybox3" href="#portfolio3-inline2"></a>
                                                <a class="portfolioFancybox hidden" data-fancybox-group="portfolioFancybox3" href="#portfolio3-inline3"></a>
                                            </div>
                                        </div>
                                    </figcaption>
                                </figure>

                                <!-- Start: Portfolio Inline Boxes -->
                                <div id="portfolio3-inline1" class="fancybox-inline-box">
                                    <img class="inline-img" src='img/uploads/portfolio/portfolio-thumb-08-large.jpg' alt=''/>                                   
                                </div>

                                <div id="portfolio3-inline2" class="fancybox-inline-box">
                                    <img class="inline-img" src='img/uploads/portfolio/portfolio-thumb-04-large.jpg' alt=''/>                                   
                                </div>
                                
                                <div id="portfolio3-inline3" class="fancybox-inline-box">
                                    <img class="inline-img" src='img/uploads/portfolio/portfolio-thumb-02-large.jpg' alt=''/>                                   
                                </div>
                                <!-- End: Portfolio Inline Boxes -->
                            </div>
                        </div><!-- .grid-item -->
                    </div>

                    <div class="grid-more">
                        <span class="ajax-loader"></span>
                        <button class="btn btn-border ripple"><i class="icon icon-add"></i></button>
                    </div>
                </div>  
            </section><!-- #portfolio -->
            
            <section id="experience" class="section section-experience">
                <div class="animate-up">
                    <h2 class="section-title animate">Work Experience</h2>

                    <div class="timeline">
                        <div class="timeline-bar"></div>
                        <div class="timeline-inner clearfix">
                            <div class="timeline-box timeline-box-left">
                                <span class="dot"></span>
                                <div class="timeline-box-inner animate-right">
                                    <span class="arrow"></span>
                                    <div class="date">1998 - 2001</div>
                                    <h3>Web Developer</h3>
                                    <h4>Web Design Company - Birmingham</h4>
                                    <p>Present Responsible for working on a range of projects, designing appealing websites and interacting on a daily basis with graphic designers, back-end developers and marketers.</p>
                                </div>
                            </div>
                            
                            <div class="timeline-box timeline-box-right">
                                <span class="dot"></span>
                                <div class="timeline-box-inner animate-left">
                                    <span class="arrow"></span>
                                    <div class="date">Jul 2001- Nov 2003</div>
                                    <h3>Trainee Web Developer</h3>
                                    <h4>SEO Company - Coventry</h4>
                                    <p>Worked as part of a multi-disciplinary team, carrying out ad-hoc tasks as requested by the IT Manager. Had a specific brief to ensure the websites build for customer’s precisely matched their requirements.</p>
                                </div>
                            </div>
                            
                            <div class="timeline-box timeline-box-left">
                                <span class="dot"></span>
                                <div class="timeline-box-inner animate-right">
                                    <span class="arrow"></span>
                                    <div class="date">2003 - 2006</div>
                                    <h3>Systems Analyst / Web Developer / Designer</h3>
                                    <h4>exampleofsite.com</h4>
                                    <p>Rebuilt and enhanced existing ASP B2C site with ASP.NET 2.0 Framework and tools. Technology consists of ASP.NET 2.0 (C#), IIS, Microsoft SQL Server 2005, Stored Procedures & PayPal Instant Payment Notification.</p>
                                </div>
                            </div>
                            
                            <div class="timeline-box timeline-box-right">
                                <span class="dot"></span>
                                <div class="timeline-box-inner animate-left">
                                    <span class="arrow"></span>
                                    <div class="date">2007 - 2010</div>
                                    <h3>Systems Analyst / Web Developer</h3>
                                    <h4>exampleofsite.com</h4>
                                    <p>Developed, maintained and promoted more than ten websites.Implemented and examined web software such as traffic analyzers, banner programs and onsite search engines.Exercised creative judgment in researching and recruiting promotional affiliates. Administrated the office network. Migrated hosting servers with detailed plans including simulating, testing and setting up complex configurations.</p>
                                </div>
                            </div>
                            
                            <div class="timeline-box timeline-box-left">
                                <span class="dot"></span>
                                <div class="timeline-box-inner animate-right">
                                    <span class="arrow"></span>
                                    <div class="date">2010 - up to present</div>
                                    <h3>Webmaster / Web Developer</h3>
                                    <h4>exampleofsite.com</h4>
                                    <p>Developed, managed, operated and promoted an Internet business.Handled customer support issues.Planned and managed business finances.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
            </section><!-- #experience -->
            
            <section id="education" class="section section-education">
                <div class="animate-up">
                    <h2 class="section-title animate">Education</h2>

                    <div class="timeline">
                        <div class="timeline-bar"></div>
                        <div class="timeline-inner clearfix">                       
                            <div class="timeline-box timeline-box-compact timeline-box-left">
                                <span class="dot"></span>

                                <div class="timeline-box-inner animate-right">
                                    <span class="arrow"></span>
                                    <div class="date"><span>1985 - 1987</span></div>
                                    <h3>Diploma in Web Design</h3>
                                    <h4>Coventry North College</h4>
                                </div>
                            </div>
                            
                            <div class="timeline-box timeline-box-compact timeline-box-right">
                                <span class="dot"></span>

                                <div class="timeline-box-inner animate-left">
                                    <span class="arrow"></span>
                                    <div class="date"><span>1987 - 1989</span></div>
                                    <h3>General Education / Computer Science</h3>
                                    <h4>Los Angeles Pierce College</h4>
                                </div>
                            </div>
                            
                            <div class="timeline-box timeline-box-compact timeline-box-left">
                                <span class="dot"></span>

                                <div class="timeline-box-inner animate-right">
                                    <span class="arrow"></span>
                                    <div class="date"><span>1989 - 1994</span></div>
                                    <h3>Bachelor of Arts in English Language and English Literature</h3>
                                    <h4>Sogang University</h4>
                                </div>
                            </div>
                            
                            <div class="timeline-box timeline-box-compact timeline-box-right">
                                <span class="dot"></span>

                                <div class="timeline-box-inner animate-left">
                                    <span class="arrow"></span>
                                    <div class="date"><span>1994 - 1996</span></div>
                                    <h3>B.S. Computer Science</h3>
                                    <h4>California State University</h4>
                                </div>
                            </div>
                            
                            <div class="timeline-box timeline-box-compact timeline-box-left">
                                <span class="dot"></span>

                                <div class="timeline-box-inner animate-right">
                                    <span class="arrow"></span>
                                    <div class="date"><span>1996 - 1998</span></div>
                                    <h3>Emphasis on Software Engineering and Object Oriented Programming</h3>
                                    <h4>George Washington University</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>  
            </section><!-- #education -->   

            <section id="clients" class="section section-clients">
                <div class="animate-up">
                    <h2 class="section-title">My Clients</h2>

                    <div class="clients-carousel">
                        <div class="client-logo">
                            <div class="valign-outer">
                                <div class="valign-middle">
                                    <div class="valign-inner">
                                        <a href="http://market.envato.com" target="_blank"><img src="img/uploads/logos/logo-envato.png" title="envato" alt="envato"/></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="client-logo">
                            <div class="valign-outer">
                                <div class="valign-middle">
                                    <div class="valign-inner">
                                        <a href="https://angularjs.org" target="_blank"><img src="img/uploads/logos/logo-angularjs.png" title="angular js" alt="angular js"/></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="client-logo">
                            <div class="valign-outer">
                                <div class="valign-middle">
                                    <div class="valign-inner">
                                        <a href="https://www.omniref.com/ruby/gems/teaspoon/0.7.9" target="_blank"><img src="img/uploads/logos/logo-teaspoon.png" title="teaspoon" alt="teaspoon"/></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="client-logo">
                            <div class="valign-outer">
                                <div class="valign-middle">
                                    <div class="valign-inner">
                                        <a href="https://wordpress.com" target="_blank"><img src="img/uploads/logos/logo-wordpress.png" title="wordpress" alt="wordpress"/></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="client-logo">
                            <div class="valign-outer">
                                <div class="valign-middle">
                                    <div class="valign-inner">
                                        <a href="https://evernote.com" target="_blank"><img src="img/uploads/logos/logo-evernote.png" title="evernote" alt="evernote"/></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="client-logo">
                            <div class="valign-outer">
                                <div class="valign-middle">
                                    <div class="valign-inner">
                                        <a href="http://compass-style.org" target="_blank"><img src="img/uploads/logos/logo-compass.png" title="compass" alt="compass"/></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="client-logo">
                            <div class="valign-outer">
                                <div class="valign-middle">
                                    <div class="valign-inner">
                                        <a href="http://getbootstrap.com" target="_blank"><img src="img/uploads/logos/logo-bootstrap.png" title="bootstrap" alt="bootstrap"/></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="client-logo">
                            <div class="valign-outer">
                                <div class="valign-middle">
                                    <div class="valign-inner">
                                        <a href="http://jasmine.github.io" target="_blank"><img src="img/uploads/logos/logo-jasmine.png" title="jasmine" alt="jasmine"/></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="client-logo">
                            <div class="valign-outer">
                                <div class="valign-middle">
                                    <div class="valign-inner">
                                        <a href="https://jquery.com" target="_blank"><img src="img/uploads/logos/logo-jquery.png" title="jquery" alt="jquery"/></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section><!-- #clients -->
            
            <section id="references" class="section section-references">
                <div class="animate-up">
                    <h2 class="section-title">References</h2>
                    <div class="section-box">
                        <ul class="ref-slider">
                            <li>
                                <div class="ref-box">
                                    <div class="person-speech">
                                        <p>I confirm that I have dealt with New Company Ltd since 1998. Their work has been a major factor in our 
                                        website's success, helping it to become one of the most visited resources of its kind on the Internet.</p>
                                    </div>
                                    <div class="person-info clearfix">
                                        <img class="person-img" src="img/uploads/rs-avatar-60x60.jpg" alt="Headshot">
                                        <div class="person-name-title">
                                            <span class="person-name">Alexander Jokovich</span>
                                            <span class="person-title">Modern LLC , HR</span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="ref-box">
                                    <div class="person-speech">
                                        <p>I confirm that New Company Ltd has been a customer of ours since 1998, during which time they have always made payments reliably, 
                                        in full and on time.</p>
                                    </div>
                                    <div class="person-info clearfix">
                                        <img class="person-img" src="img/uploads/rs-avatar-60x60.jpg" alt="Headshot">
                                        <div class="person-name-title">
                                            <span class="person-name">Alexander Jokovich</span>
                                            <span class="person-title">Modern LLC , HR</span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="ref-box">
                                    <div class="person-speech">
                                        <p>I have known Robert Smith for 10 years as web developer. I can confirm that he is a man of great integrity, is extremely dedicated 
                                        to his family and work, and is entirely peace-loving.</p>
                                    </div>
                                    <div class="person-info clearfix">
                                        <img class="person-img" src="img/uploads/rs-avatar-60x60.jpg" alt="Headshot">
                                        <div class="person-name-title">
                                            <span class="person-name">Alexander Jokovich</span>
                                            <span class="person-title">Modern LLC , HR</span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <div class="ref-slider-nav">
                            <span id="ref-slider-prev" class="slider-prev"></span>
                            <span id="ref-slider-next" class="slider-next"></span>
                        </div>
                    </div>
                </div>  
            </section><!-- #references -->              
            
            <section id="prices" class="section section-prices">
                <div class="animate-up">
                    <h2 class="section-title">Price List</h2>

                    <div class="row price-list">
                        <div class="col-sm-4">
                            <div class="price-box">
                                <div class="price-box-top">
                                    <span>£5</span><small>/mo</small>
                                </div>
                                <div class="price-box-content">
                                    <h3>Membership</h3>
                                    <ul>
                                        <li>Lorem Ipsum Servise</li>
                                        <li>12 Ipsum Servise</li>
                                        <li>Lorem Ipsum Servise 6</li>
                                        <li>New Servise 2I</li>
                                        <li>Servise</li>
                                        <li>Ipsum Servise 2</li>
                                    </ul>
                                    <a class="btn btn-lg btn-border" href="">Buy Now</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="price-box box-primary">
                                <div class="price-box-top">
                                    <span>£25</span><small>/mo</small>
                                </div>
                                <div class="price-box-content">
                                    <h3>Standard Website</h3>
                                    <ul>
                                        <li>Lorem Ipsum Servise</li>
                                        <li>48 Ipsum Servise 2</li>
                                        <li>Lorem Ipsum Servise 6</li>
                                        <li>New Servise 2I</li>
                                        <li>Servise</li>
                                        <li>Ipsum Servise 2</li>
                                    </ul>
                                    <a class="btn btn-lg btn-border" href="">Buy Now</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="price-box">
                                <div class="price-box-top">
                                    <span>£50</span><small>/mo</small>
                                </div>
                                <div class="price-box-content">
                                    <h3>Ecommerce Website</h3>
                                    <ul>
                                        <li>Lorem Ipsum Servise</li>
                                        <li>Unlimited Servise 2</li>
                                        <li>Lorem Ipsum Servise 6</li>
                                        <li>New Servise 2I</li>
                                        <li>Servise</li>
                                        <li>Ipsum Servise 2</li>
                                    </ul>
                                    <a class="btn btn-lg btn-border" href="">Buy Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section><!-- #prices -->                                                                                          
            
            <section id="blog" class="section section-blog">
                <div class="animate-up">
                    <h2 class="section-title">From The Blog</h2>

                    <div class="row">
                        <div class="col-xs-6">
                            <article class="post-box">
                                <div class="post-media">
                                    <div class="post-image">
                                        <a href="single.html"><img src="img/uploads/thumb-449x286-1.jpg" alt=""> </a>
                                    </div>
                                </div>

                                <div class="post-data">
                                    <time class="post-datetime" datetime="2015-03-13T07:44:01+00:00">
                                        <span class="day">03</span>
                                        <span class="month">MAY</span>
                                    </time>

                                    <div class="post-tag">
                                        <a href="">#Photo</a>
                                        <a href="">#Architect</a>
                                    </div>

                                    <h3 class="post-title">
                                        <a href="single-image.html">Image Post</a>
                                    </h3>

                                    <div class="post-info">
                                        <a href=""><i class="icon icon-user"></i>by admin</a>
                                        <a href=""><i class="icon icon-comments"></i>56</a>
                                    </div>
                                </div>
                            </article>
                        </div>
                        
                        <div class="col-xs-6">
                            <article class="post-box">
                                <div class="post-media">
                                    <div class="post-image">
                                        <a href="single-vimeo.html">
                                            <img src="img/uploads/thumb-449x286-5.jpg" alt="">
                                            <span class="post-type-icon"><i class="icon icon-play"></i></span>
                                        </a>
                                    </div>
                                </div>

                                <div class="post-data">
                                    <time class="post-datetime" datetime="2015-03-13T07:44:01+00:00">
                                        <span class="day">03</span>
                                        <span class="month">MAY</span>
                                    </time>

                                    <div class="post-tag">
                                        <a href="">#Photo</a>
                                        <a href="">#Architect</a>
                                    </div>

                                    <h3 class="post-title">
                                        <a href="single-vimeo.html">Vimeo Video Post</a>
                                    </h3>

                                    <div class="post-info">
                                        <a href=""><i class="icon icon-user"></i>by admin</a>
                                        <a href=""><i class="icon icon-comments"></i>56</a>
                                    </div>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>  
            </section><!-- #blog -->
            
            <section id="interests" class="section section-interests">
                <div class="animate-up">
                    <h2 class="section-title">My Interests</h2>

                    <div class="section-box">
                        <p>I have a keen interest in photography. I was vice-president of the photography club during my time at university, 
                        and during this period I organised a number of very successful exhibitions and events both on and off campus.                       
                        <br/>I also play the piano to grade 8 standard.</p>

                        <ul class="interests-list">
                            <li>
                                <i class="map-icon map-icon-bicycling"></i>
                                <span>Bicycling</span>
                            </li>
                            <li>
                                <i class="map-icon map-icon-movie-theater"></i>
                                <span>Watch Movies</span>
                            </li>
                            <li>
                                <i class="map-icon map-icon-ice-skating"></i>
                                <span>Skating</span>
                            </li>
                            <li>
                                <i class="map-icon map-icon-shopping-mall"></i>
                                <span>Shopping</span>
                            </li>
                            <li>
                                <i class="map-icon map-icon-tennis"></i>
                                <span>Playing Tennis</span>
                            </li>
                            <li>
                                <i class="map-icon map-icon-bowling-alley"></i>
                                <span>Playing Bowling</span>
                            </li>
                            <li>
                                <i class="map-icon map-icon-swimming"></i>
                                <span>Swimming</span>
                            </li>
                        </ul>
                    </div>
                </div>  
            </section><!-- #interests -->
            
            <section id="calendar" class="section section-calendar">
                <div class="animate-up">
                    <h2 class="section-title">Availability Calendar</h2>

                    <div class="calendar-busy">
                        <div class="calendar-today" style="background-image: url('img/uploads/rs-calendar-cover.jpg')">
                            <div class="valign-outer">
                                <div class="valign-middle">
                                    <div class="valign-inner">
                                        <div class="date">
                                            <span class="day"></span>
                                            <span class="month"></span>
                                        </div>
                                        <div class="week-day"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="calendar-cont">
                            <div class="calendar-header">
                                <div class="calendar-nav">
                                    <span class="active-date"><span class="active-month"></span><span class="active-year"></span></span>
                                    <a class="calendar-prev ripple-centered" title="Prev"><i class="icon icon-chevron_left"></i></a>
                                    <a class="calendar-next ripple-centered" title="Next"><i class="icon icon-chevron_right"></i></a>
                                </div>
                            </div>

                            <table class="calendar-body">
                                <thead class="calendar-thead"></thead>
                                <tbody class="calendar-tbody"></tbody>
                            </table>
                            <div class="calendar-busy-note">Sorry. I'm not available on those days</div>
                        </div>
                    </div>
                </div>  
            </section><!-- #calendar -->
            
            <section id="contact" class="section section-contact">
                <div class="animate-up">
                    <h2 class="section-title">Contact Me</h2>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="section-box contact-form">
                                <h3>Feel free to contact me</h3>
                                
                                <form class="contactForm" action="php/contact_form.php" method="post">
                                    <div class="input-field">
                                        <input class="contact-name" type="text" name="name"/>
                                        <span class="line"></span>
                                        <label>Name</label>
                                    </div>

                                    <div class="input-field">
                                        <input class="contact-email" type="email" name="email"/>
                                        <span class="line"></span>
                                        <label>Email</label>
                                    </div>

                                    <div class="input-field">
                                        <input class="contact-subject" type="text" name="subject"/>
                                        <span class="line"></span>
                                        <label>Subject</label>
                                    </div>

                                    <div class="input-field">
                                        <textarea class="contact-message" rows="4" name="message"></textarea>
                                        <span class="line"></span>
                                        <label>Message</label>
                                    </div>

                                    <span class="btn-outer btn-primary-outer ripple">
                                        <input class="contact-submit btn btn-lg btn-primary" type="submit" value="Send"/>
                                    </span>
                                    
                                    <div class="contact-response"></div>
                                </form>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="section-box contact-map">
                                <dl class="contact-info">
                                    <dt>ADDRESS</dt>
                                    <dd>Belgium, Brussels, Liutte 27, BE</dd>
                                    <dt>phone</dt>
                                    <dd><a href="tel:+12562548456">+1 256 254 84 56</a></dd>
                                    <dt>E-mail</dt>
                                    <dd><a href="mailto:robertsmith@company.com">robertsmith@company.com</a></dd>
                                </dl>

                                <div id="map" data-latitude="50.84592" data-longitude="4.366859999999974"></div>
                            </div>
                        </div>
                    </div>
                </div>  
            </section><!-- #contact -->
        <!-- END: PAGE CONTENT -->
            
        </div><!-- .container -->
    </div><!-- .content -->

@endsection