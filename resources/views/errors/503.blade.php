@extends('layouts.main')

@section('title', '503')

@section('description', '503')
@section('keywords', '503')

@section('content')

        <div class="content">
            <div class="container">
            
            <!-- START: PAGE CONTENT -->
            <div class="page-404">
                <h2>5<span>0</span>3</h2>
                <p>Ooops! This service is unavailable!</p>
                <a class="btn btn-lg btn-border" href="/">Go To The Homepage ?</a>
            </div>
            <!-- END: PAGE CONTENT -->
                
            </div><!-- .container -->
        </div><!-- .content -->

@endsection