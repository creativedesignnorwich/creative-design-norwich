<!DOCTYPE html>
<html lang="en" class="theme-color-33d685 theme-skin-light">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Creative Design Norwich - Adam Wilson Freelancer Developer - @yield('title')</title>
    <meta name="author" content="Adam Wilson">
	<meta name="description" content="@yield('description')">
	<meta name="keywords" content="@yield('keywords')">
    <link rel="shortcut icon" type="image/ico" href="/img/favicon.png"/>
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Fredoka+One">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic">
    <link rel="stylesheet" type="text/css" href="/fonts/map-icons/css/map-icons.min.css">
    <link rel="stylesheet" type="text/css" href="/fonts/icomoon/style.css">
    <link rel="stylesheet" type="text/css" href="/js/plugins/jquery.bxslider/jquery.bxslider.css">
    <link rel="stylesheet" type="text/css" href="/js/plugins/jquery.customscroll/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" type="text/css" href="/js/plugins/jquery.mediaelement/mediaelementplayer.min.css">
    <link rel="stylesheet" type="text/css" href="/js/plugins/jquery.fancybox/jquery.fancybox.css">
    <link rel="stylesheet" type="text/css" href="/js/plugins/jquery.owlcarousel/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="/js/plugins/jquery.owlcarousel/owl.theme.css">
    <link rel="stylesheet" type="text/css" href="/style.css">
    <link rel="stylesheet" type="text/css" href="/green.css">
    <script type="text/javascript" src="js/libs/modernizr.js"></script>
</head>

<body class="home header-has-img loading">

    <div class="mobile-nav">
        <button class="btn-mobile mobile-nav-close"><i class="icon icon-close"></i></button>
        
        <div class="mobile-nav-inner">
            <nav class="nav">
                <ul class="clearfix">
                    <li><a href="/#about">About</a></li>
                    <li><a href="/#skills">Skills</a></li>
                    <li><a href="/#portfolio">Portfolio</a> </li>
                    <li><a href="/#experience">Experience</a></li>
                    <li><a href="/#references">References</a></li>
                    <!-- <li><a href="/blog">Blog</a></li> -->
                    <li><a href="/#calendar">Calendar <span></span></a></li>
                    <li><a href="/#contact">Contact <span></span></a></li>
                </ul>
            </nav>
        </div>
    </div><!-- .mobile-nav -->

    <div class="wrapper">
        <header class="header">
            <div class="head-bg" style="background-image: url('img/uploads/rs-cover.jpg')"></div>

            <div class="head-bar">
                <div class="head-bar-inner">
                    <div class="row">
                        <div class="col-sm-3 col-xs-6">                            
                            <a class="logo" href="index.html"><span>CD</span>NORWICH</a>
                            <!-- <a class="head-logo" href=""><img src="img/rs-logo.png" alt="RScard"/></a> -->
                        </div>

                        <div class="col-sm-9 col-xs-6">
                            <div class="nav-wrap">
                                <nav class="nav">
                                    <ul class="clearfix">
                                        <li><a href="/#about">About</a></li>
					                    <li><a href="/#skills">Skills</a></li>
					                    <li><a href="/#portfolio">Portfolio</a> </li>
					                    <li><a href="/#experience">Experience</a></li>
					                    <li><a href="/#references">References</a></li>
					                    <!-- <li><a href="/blog">Blog</a></li> -->
					                    <li><a href="/#calendar">Calendar <span></span></a></li>
					                    <li><a href="/#contact">Contact <span></span></a></li>
                                    </ul>
                                </nav>

                                <button class="btn-mobile btn-mobile-nav">Menu</button>
                            </div><!-- .nav-wrap -->
                        </div>
                    </div>
                </div>
            </div>
        </header><!-- .header -->

        @yield('content')

        <footer class="footer">
            <div class="footer-social">
                <ul class="social">
                    <li><a class="ripple-centered" href="https://www.facebook.com/08wilsona" target="_blank"><i class="icon icon-facebook"></i></a></li>
                    <li><a class="ripple-centered" href="https://twitter.com//its_adam97" target="_blank"><i class="icon icon-twitter"></i></a></li>
                    <li><a class="ripple-centered" href="https://uk.linkedin.com/in/08wilsona" target="_blank"><i class="icon icon-linkedin"></i></a></li>
                    <li><a class="ripple-centered" href="https://www.instagram.com/itsadam97" target="_blank"><i class="icon icon-instagram"></i></a></li>
                </ul>
            </div>
        </footer><!-- .footer -->
    </div><!-- .wrapper -->
    
    <a class="btn-scroll-top" href="#"><i class="icon icon-arrow-up"></i></a>
    <div id="overlay"></div>
    <div id="preloader">
        <div class="preload-icon"><span></span><span></span></div>
        <div class="preload-text">Loading ...</div>
    </div>

    <!-- Scripts -->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>
    <script type="text/javascript" src="fonts/map-icons/js/map-icons.min.js"></script>
    <script type="text/javascript" src="js/plugins/jquery.mousewheel-3.0.6.pack.js"></script>
    <script type="text/javascript" src="js/plugins/imagesloaded.pkgd.min.js"></script>
    <script type="text/javascript" src="js/plugins/isotope.pkgd.min.js"></script>
    <script type="text/javascript" src="js/plugins/jquery.appear.min.js"></script>
    <script type="text/javascript" src="js/plugins/jquery.bxslider/jquery.bxslider.min.js"></script>
    <script type="text/javascript" src="js/plugins/jquery.customscroll/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="js/plugins/jquery.mediaelement/mediaelement-and-player.min.js"></script>
    <script type="text/javascript" src="js/plugins/jquery.fancybox/jquery.fancybox.pack.js"></script>
    <script type="text/javascript" src="js/plugins/jquery.fancybox/helpers/jquery.fancybox-media.js"></script>
    <script type="text/javascript" src="js/plugins/jquery.owlcarousel/owl.carousel.min.js"></script>
    <script type="text/javascript" src="js/site.min.js"></script>
</body>
</html>

